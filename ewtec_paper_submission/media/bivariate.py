import numpy as np
import pylab as pl
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm
import matplotlib.mlab as mlab

minx, maxx = -5, 5
miny, maxy = -5, 5
delta = 0.025

y1 = 1.0

mean = [0, 0]
sigma_xx = 2
sigma_yy = 1
sigma_xy = 0.9

y = np.arange(miny, maxy, delta)
X, Y = np.mgrid[minx:maxx:delta, miny:maxy:delta]

#pos = np.empty(x.shape + (2,))
#pos[:, :, 0] = x 
#pos[:, :, 1] = y

Z = mlab.bivariate_normal(X, Y, sigma_xx, sigma_yy, mean[0], mean[1], sigma_xy)

fig = pl.figure()

y_cond = Y[min(enumerate(y), key=lambda x: abs(x[1]-y1))[0]]
x_cond = X[min(enumerate(y), key=lambda x: abs(x[1]-y1))[0]]
z_cond = Z[min(enumerate(y), key=lambda x: abs(x[1]-y1))[0]]
#z_cond +=0.01

#ax = fig.add_subplot(111)
#ax.contourf(x, y, rv.pdf(pos))

# Plot the bisecting plane
#z = (x - y)

ax = fig.gca(projection='3d')

ax.set_xlim(minx, maxx)
ax.set_ylim(miny, maxy)
#ax.set_zlim(0, 0.1)

ax.plot_surface(X, Y, Z, rstride=12, cstride=12, alpha=0.5, cmap=cm.coolwarm)
#ax.plot_surface(x_cond,y_cond,z_cond, rstride=8, cstride=8, alpha=0.5,cmap=cm.coolwarm)
ax.plot(x_cond, y_cond, z_cond , color='red', linewidth=4)
ax.plot(x_cond, y_cond, color='black', linewidth=6)

ax.set_xlabel('$x_1$')
ax.set_ylabel('$x_2$')
ax.set_zlabel('$y$')


pl.savefig('bivariate_graph.png', dpi=300)
#pl.show()
